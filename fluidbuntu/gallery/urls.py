# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url

from . import views

urlpatterns = [
    # URL pattern for the UserListView
    url(
        regex=r'^$',
        view=views.index,
        name='index'
    ),
    # Author list
    url(
        regex=r'^authors/$',
        view=views.author_list,
        name='author_list'
    ),

    # filter by author URL
    url(
        regex=r'^author/(?P<slug>[-\w\d]+)-(?P<id>\d+)/$',
        view=views.author,
        name='author'
    ),

    # filter by date
    url(
        regex=r'^date',
        view=views.date,
        name='date'
    ),
    url(
        regex=r'^(?P<date_range>20\d{2}(/\d{1,2})?(/\d{1,2})?)$',
        view=views.date_range,
        name='date_range'
    ),


    url(
        regex=r'^pic/(?P<uslug>[-\w\d]+)-(?P<id>[\d_]+)/$',
        view=views.single_photo,
        name='single_photo'
    ),

]
