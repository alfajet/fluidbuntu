from django.core.management.base import BaseCommand, CommandError
from ...utils.managephotos import Resize


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('photoid', nargs='+', type=str)

    def handle(self, *args, **options):
        for photoid in options['photoid']:
            Resize(photoid, 256, 256)
