from django.core.management.base import BaseCommand  #, CommandError
from ...models import Topic
import logging

logger = logging.getLogger(__name__)

class Command(BaseCommand):


    def handle(self, *args, **options):
        """
        Check for new photos to be imported in each topic.
        """
        for t in Topic.objects.all():
            t.run_import()
