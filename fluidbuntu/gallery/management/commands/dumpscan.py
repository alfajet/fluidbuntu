from django.core.management.base import BaseCommand  #, CommandError
from ...utils import scanforum
import logging

logger = logging.getLogger(__name__)

class Command(BaseCommand):


    def add_arguments(self, parser):
        parser.add_argument('url', nargs=1, type=str)

    def handle(self, *args, **options):

        for url in options['url']:
            photos = scanforum.scan_forum(url)
            if photos:
                self.stdout.write('Photos: {0}'.format(len(photos)))
                for idx, photo_post in enumerate(photos):
                    self.stdout.write('{0}\tAuthor: {1}, Count photos: {2}'.format(idx, photo_post['user_name'],
                                                                             len(photo_post['photos'])))
