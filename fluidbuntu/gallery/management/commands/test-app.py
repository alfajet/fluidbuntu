from django.core.management.base import BaseCommand, CommandError
from ...utils.managephotos import Resize
from ...utils.managephotos import fetch_photo
from PIL import Image

import logging

logger = logging.getLogger(__name__)

class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--testcase', nargs=1, type=str)

    def test_scanforum(self):
        pass

    def test_fetchphoto(self):
        self.stdout.write('Testing getting a photo...')
        photo_url = 'http://pix.tdct.org/upload/img/1472044933.jpg'
        photo_link_url = 'http://pix.tdct.org/?img=1472044933.jpg'

        img = fetch_photo(photo_url, photo_link_url)
        if img:
            self.stdout.write('We\'ve got the photo!!' )
        else:
            self.stdout.write("The photo didn't load")

    def handle(self, *args, **options):
        test_cases = {
            'scan-forum': self.test_scanforum,
            'fetch-photo': self.test_fetchphoto,
        }
        test_cases[str(options['testcase'][0])]()
