from django.core.management.base import BaseCommand, CommandError
from ...models import Photo
import logging
# from ...utils.managephotos import Resize
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-p', '--photo-id', nargs='+', type=str,
                            dest='photo_id')
        parser.add_argument('-t', '--post-id', nargs='+', type=str,
                            dest='post_id')

    def handle(self, *args, **options):
        photos_to_delete = []
        if options['photo_id']:
            for photo_id in options['photo_id']:
                fetched_photos = Photo.objects.all().filter(
                    id=photo_id)
                if fetched_photos:
                    photos_to_delete.extend(fetched_photos)
                else:
                    self.stdout.write('Impossible to find {}'.format(photo_id))
        if options['post_id']:
            for post_id in options['post_id']:
                fetched_photos = Photo.objects.all().filter(
                    post_id=post_id)
                if fetched_photos:
                    photos_to_delete.extend(fetched_photos)
                else:
                    self.stdout.write('Impossible to find {}'.format(post_id))
        if photos_to_delete:
            for p in photos_to_delete:
                logger.info('deleting photo {}'.format(p.post_url))
                p.delete()
