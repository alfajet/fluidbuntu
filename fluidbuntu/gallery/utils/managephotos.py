# -*- coding: utf-8 -*-
from __future__ import division

from fileinput import filename

try:
    from urlparse import urlparse, urljoin
except:
    from urllib.parse import urlparse
from cStringIO import StringIO
# from urllib2 import urlopen
import requests
from PIL import Image, ExifTags
import piexif
import logging
import re
from django.core.files.base import ContentFile
from datetime import datetime

# logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
logger = logging.getLogger(__name__)

SUPPORTED_IMAGEFILE_EXTENSIONS = ['.jpg', '.jpeg', '.png', '.gif', '.webp',
                                  '.bmp']

class EXIF_TAG_NAMES:
    Model = 'Model'
    Make = 'Make'
    Copyright = 'Copyright'
    Focal_length = 'Focal_length'
    Focal_length_35 = 'Focal_length_35'
    Exposure_time = 'Exposure_time'
    Aperture = 'Aperture'
    ISO = 'ISO'
    Caption = 'Caption'
    Comments = 'Comments'
    Capture_date = 'Capture_date'
    Lat_ref = 'Lat_ref'
    Lat = 'Lat'
    Long_ref = 'Long_ref'
    Long = 'Long'

class EXIF_DATA_TYPE:
    Short_text = 0
    Long_text = 1
    Point = 2
    DateTime = 3

EXIF_TAGS = {
    EXIF_TAG_NAMES.Model: {'segment': '0th', 'property':
        piexif.ImageIFD.Model, 'type': EXIF_DATA_TYPE.Short_text},
    EXIF_TAG_NAMES.Make: {'segment': '0th', 'property': piexif.ImageIFD.Make,
                          'type': EXIF_DATA_TYPE.Short_text},
    EXIF_TAG_NAMES.Copyright: {'segment': '0th', 'property':
        piexif.ImageIFD.Copyright, 'type': EXIF_DATA_TYPE.Short_text},
    EXIF_TAG_NAMES.Focal_length: {'segment': 'Exif',
                     'property': piexif.ExifIFD.FocalLength, 'type':
                                      EXIF_DATA_TYPE.Point},
    EXIF_TAG_NAMES.Focal_length_35: {'segment': 'Exif', 'property':
        piexif.ExifIFD.FocalLengthIn35mmFilm, 'type': EXIF_DATA_TYPE.Point},
    EXIF_TAG_NAMES.Exposure_time: {'segment': 'Exif', 'property':
        piexif.ExifIFD.ExposureTime, 'type': EXIF_DATA_TYPE.Point},
    EXIF_TAG_NAMES.Aperture: {'segment': 'Exif', 'property':
        piexif.ExifIFD.ApertureValue, 'type': EXIF_DATA_TYPE.Point},
    EXIF_TAG_NAMES.ISO: {'segment': 'Exif', 'property':
        piexif.ExifIFD.ISOSpeed, 'type': EXIF_DATA_TYPE.Point},
    EXIF_TAG_NAMES.Caption: {'segment': '0th',
                'property': piexif.ImageIFD.ImageDescription, 'type':
                                 EXIF_DATA_TYPE.Short_text},
    EXIF_TAG_NAMES.Comments: {'segment': 'Exif', 'property':
        piexif.ExifIFD.UserComment, 'type': EXIF_DATA_TYPE.Long_text},
    EXIF_TAG_NAMES.Capture_date:
        {'segment': '0th', 'property': piexif.ExifIFD.DateTimeOriginal,
         'type': EXIF_DATA_TYPE.DateTime},
    EXIF_TAG_NAMES.Lat_ref: {'segment': 'GPS', 'property':
        piexif.GPSIFD.GPSLatitudeRef, 'type': EXIF_DATA_TYPE.Short_text},
    EXIF_TAG_NAMES.Lat: {'segment': 'GPS', 'property':
        piexif.GPSIFD.GPSLatitude, 'type': EXIF_DATA_TYPE.Point},
    EXIF_TAG_NAMES.Long_ref: {'segment': 'GPS', 'property':
        piexif.GPSIFD.GPSLongitudeRef, 'type': EXIF_DATA_TYPE.Short_text},
    EXIF_TAG_NAMES.Long: {'segment': 'GPS', 'property':
        piexif.GPSIFD.GPSLongitude, 'type': EXIF_DATA_TYPE.Point},
}


class InvalidUrlError(ValueError):
    pass


class InvalidPhotoError(ValueError):
    pass


def set_photo_filename(photo_id, res, extension):
    """
    :param photo_id: Photo ID, used to define file name
    :param res: lr or hr image type
    :param extension: photo extension
    :return: file path
    :rtype: str
    """
    filename = '{0}_{1}.{2}'.format(photo_id, res, extension)
    return filename


# def set_upload_path_hr(instance, filename=None):
#     """
#     :type instance: Photo
#     :type filename: str
#     :rtype: str
#     """
#     try:
#         date = instance.post_date
#     except:
#         date = datetime.now()
#     return set_upload_path(instance.id, 'hr', date)
#
#
# def set_upload_path_lr(instance, filename=None):
#     """
#     :type instance: Photo
#     :type filename: str
#     :rtype: str
#     """
#     try:
#         date = instance.post_date
#     except:
#         date = datetime.now()
#     return set_upload_path(instance.id, 'lr', date)


def process_pix(photo_link_url):
    regex = re.compile(r'\d+\.\w+')
    match = regex.search(photo_link_url)
    if match:
        parsed_url = urlparse(photo_link_url)
        pict_url = urljoin(parsed_url.scheme + '://' + parsed_url.netloc,
                           '/upload/original/' + match.group())
        return pict_url
    else:
        raise InvalidUrlError(
            'Invalid PIX picture url: {0}'.format(photo_link_url))


def process_piwigo(photo_url):
    regex = re.compile(r'^(.+)-\w{2}(\.jpg)$')
    if regex.match(photo_url):
        new_url = regex.sub(r'\1\2', photo_url).replace('i.php?/', '')
        return new_url
    else:
        raise InvalidUrlError('Invalid Piwigo Url Error:{}'.format(photo_url))


def process_default(photo_link_url, photo_url):
    """
    :type photo_link_url: str
    :param photo_link_url:
    :return:
    """
    urls = (photo_link_url, photo_url)
    for url in urls:
        logger.debug('URL Testing: {0}'.format(url))
        logger.debug('URL Matches file extensions? {0}'.format(
            url.lower().endswith(tuple(SUPPORTED_IMAGEFILE_EXTENSIONS))))
        pattern = r"/[\w\.\-\_\(\)\[\]]+$"
        logger.debug('URL file pattern? {0}'.format(
            re.search(pattern, url)))
        if url.lower().endswith(tuple(SUPPORTED_IMAGEFILE_EXTENSIONS)) \
            and re.search(pattern, url):
            # first test checks the extension
            # second checks that the url is actually a file name
            return url
    return None


def get_original_photo_url(photo_url, photo_link_url):
    """
    :type photo_link_url: str
    :param photo_link_url:
    :return: PIL.Image
    """
    gallery_functions = {
        'pix.tdct.org': (process_pix, photo_url),
        'pix.toile-libre.org': (process_pix, photo_url),
        'macahute.net': (process_piwigo, photo_url),
    }
    parsed_url = urlparse(photo_url)
    if parsed_url.netloc in gallery_functions.keys():
        try:
            new_url = gallery_functions[parsed_url.netloc][0](
                gallery_functions[parsed_url.netloc][1])
            return new_url
        except InvalidUrlError as err:
            logger.warn(str(err))
            raise
    else:
        new_url = process_default(photo_link_url, photo_url)
        if new_url:
            return new_url
        else:
            raise InvalidUrlError(
                'Invalid url, impossible to get image from {0}: {1}, {2}'.format(
                    new_url, photo_url, photo_link_url))


def fetch_photo(photo_url, photo_link_url):
    """
    :param photo_url:
    :param photo_link_url:
    :return:
    :rtype: Image.Image, str
    """
    try:
        url = get_original_photo_url(photo_url, photo_link_url)
    except:
        raise
    else:
        logger.debug('Url to be requested: {0}, sources: {1}, {2}'.format(
            url, photo_url, photo_link_url))
        # img_ext = re.search(r'\.(\w+)$', url).group(1)
        resp_head = requests.head(url)
        logger.debug('Request header {0}'.format(resp_head.headers))
        logger.info('Loading {0}...'.format(url))
        try:
            response = requests.get(url)
            # logger.info('response: {0}'.format(response.content))
            img = Image.open(StringIO(response.content))
            img_ext = img.format
            return img, img_ext
        except:
            raise InvalidPhotoError


def define_new_size(src_size, size):
    """
    :type src_size: tuple[1]
    :type size: int
    :param src_size:
    :param size:
    :return: calculated size
    :rtype: int
    """
    is_portrait = (src_size[0] < src_size[1])

    if is_portrait:
        ratio = src_size[1] / size
    else:
        ratio = src_size[0] / size

    logger.debug('Resize ratio: {0}'.format(ratio))
    if ratio < 1:
        new_size = src_size  # no need to resize
    else:
        new_size = (int(src_size[0] / ratio), int(src_size[1] / ratio))
    return new_size


def resize_photo(photo, size):
    """
    :type photo: Image.Image
    :param photo:
    :param size:
    :type size: int
    :return: resized Image
    :rtype: Image.Image
    """

    src_size = photo.size
    new_size = define_new_size(src_size, size)
    if new_size == src_size:
        return photo
    else:
        new_photo = photo.resize(new_size, Image.LANCZOS)
        return new_photo


def pil_to_django(image, file_format, exif):
    """
    :param image: PIL Image object to be saved in a file (StringIO) object
    :type image: Image.Image
    :param file_format: file extension
    :type file_format: str
    :param exif: exif to be saved withe the new picture
    :type exif: dict or None
    :return: ContentFile object that can be saved into a FileField
    """
    # http://stackoverflow.com/questions/3723220/how-do-you-convert-a-pil-image-to-a-django-file
    fobject = StringIO()
    try:
        exif_bytes = piexif.dump(exif)
    except:
        exif_bytes = None
    finally:
        if file_format.lower() in ['jpg', 'jpeg']:
            if exif_bytes:
                try:
                    image.save(fobject, format=file_format, exif=exif_bytes,
                               quality=90, subsampling=0)
                except:
                    image.save(fobject, format=file_format, quality=90,
                               subsampling=0)
            else:
                image.save(fobject, format=file_format, quality=90,
                           subsampling=0)
        else:
            if exif_bytes:
                try:
                    image.save(fobject, format=file_format, exif=exif_bytes)
                except:
                    image.save(fobject, format=file_format)
            else:
                image.save(fobject, format=file_format)
        try:
            return ContentFile(fobject.getvalue())
        except:
            raise InvalidPhotoError


def get_exif(img):
    """
    Extract EXIF from source image if appropriate
    :param img: Source image
    :type img: Image.Image
    :return: None or EXIF bytes
    :rtype: None or dict
    """
    try:
        exif = piexif.load(img.info["exif"])
        return exif
    except:
        return None


def rotate_image(img, exif):
    """
    :param img: source image, to be rotated if orientation is present in exif
    :type img: Image.Image
    :param exif: source exif in a dict format
    :return: rotated image, and updated exif
    :rtype: Image.Image, dict
    """
    if piexif.ImageIFD.Orientation in exif["0th"]:
        orientation = exif["0th"][piexif.ImageIFD.Orientation]
        logger.info('Orientation: {0}'.format(orientation))
        if orientation == 2:
            img = img.transpose(Image.FLIP_LEFT_RIGHT)
        elif orientation == 3:
            img = img.rotate(180, expand=1)
        elif orientation == 4:
            img = img.rotate(180, expand=1).transpose(Image.FLIP_LEFT_RIGHT)
        elif orientation == 5:
            img = img.rotate(-90, expand=1).transpose(Image.FLIP_LEFT_RIGHT)
        elif orientation == 6:
            img = img.rotate(-90, expand=1)
        elif orientation == 7:
            img = img.rotate(90, expand=1).transpose(Image.FLIP_LEFT_RIGHT)
        elif orientation == 8:
            img = img.rotate(90, expand=1)
            exif["0th"].pop(piexif.ImageIFD.Orientation)

    return img, exif


def get_photo_files(photo_id, photo_url, photo_link_url, sizes):
    """
    :param photo_id: Photo ID, used for the name
    :param photo_url: Forum image, as displayed in the post
    :param photo_link_url: When available, link to the source image in order
    to get the high resolution file.
    :param sizes: list of image sizes to generate
    :return: a dict of photo tuples (filename, content file object) and an
    exif dict.
    """
    try:
        img, img_ext = fetch_photo(photo_url, photo_link_url)
    except:
        raise
    else:
        exif = get_exif(img)
        exif_data = get_exif_data(exif)
        if exif is not None:
            img, exif = rotate_image(img, exif)
        photo_files = {}
        for size in sizes:
            resized_img = resize_photo(img, sizes[size])
            # exif = get_exif(img)
            # new_img, new_exif = rotate_image(resized_img, exif)
            # new_img, new_exif = resized_img, exif
            photo_filename = set_photo_filename(photo_id, size,
                                                img_ext.lower())
            photo_files[size] = (
                (photo_filename, pil_to_django(resized_img, img_ext, exif))
            )
        return photo_files, exif_data


def get_exif_property(exif, segment, property):
    """
    Get value from an exif property and returns it as a string, none otherwise.

    :param exif: exif object
    :type exif: dict
    :param segment: location of exif property
    :type segment: str
    :param property: property ID
    :return: a property or None
    """
    if property in exif[segment]:
        return exif[segment][property]
    else:
        return None


def get_exif_data(exif):
    """
    Extract values from an Exif and return them to a dictionary
    :param exif:
    :return: a dict
    :rtype: dict
    """
    exif_dict = {}
    for key in EXIF_TAGS:
        prop = get_exif_property(exif, EXIF_TAGS[key]['segment'],EXIF_TAGS[
            key]['property'])
        if prop:
            exif_dict[key] = prop
    return prop

