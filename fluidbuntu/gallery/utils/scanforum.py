# -*- coding: utf-8 -*-
# from ..models import Photo
from bs4 import BeautifulSoup, Tag
import logging
import time
import urllib2
from urllib2 import HTTPError
import re
from datetime import datetime, timedelta
from django.utils import six

logger = logging.getLogger(__name__)

re_today = re.compile(ur'Aujourd.+?à (?P<hour>\d{2}):(?P<minute>\d{2})', re.UNICODE)
re_yesterday = re.compile(ur'Hier.+?(?P<hour>\d{2}):(?P<minute>\d{2})', re.UNICODE)
re_general_date = re.compile(
    ur'.+?(?P<day>\d{2})/(?P<month>\d{2})/(?P<year>\d{4}),'
    ur'.+?(?P<hour>\d{2}):(?P<minute>\d{2})', re.UNICODE)


def parse_topic_date(value):
    """
    :type value: unicode
    :param value:
    :rtype: datetime | None
    """
    date_patterns = {
        'today': re_today,
        'yesterday': re_yesterday,
        'general_date': re_general_date,
    }
    for key in date_patterns.keys():
        match = date_patterns[key].match(value)
        logger.debug('Pattern  {0}, Match {1}'.format(
            date_patterns[key].pattern.encode('utf-8'), match))
        if match:
            kw = {k: int(v) for k, v in six.iteritems(match.groupdict())}
            if key == 'today':
                kw['year'], kw['month'], kw['day'] = datetime.now().year, \
                    datetime.now().month, datetime.now().day
            elif key == 'yesterday':
                yesterday = datetime.today() - timedelta(days=1)
                kw['year'], kw['month'], kw['day'] = yesterday.year, \
                    yesterday.month,  yesterday.day

            kw.update({'second': 0, 'microsecond': 0})
            post_date = datetime(**kw)
            logger.debug('Computed date: {0}'.format(post_date))
            return post_date
    return None


def get_page(url):
    """
    :type url: str
    :param url: url of the page
    :return: a beautiffulsoup representation of the current forum
    page
    """
    for i in range(5):
        try:
            page = BeautifulSoup(urllib2.urlopen(url), "html.parser")
            break
        except HTTPError as inst:
            if inst.code == 404:
                raise
        except:
            logger.error('essai: {0}, {1}'.format(i, url))
            if i == 14:
                raise
            time.sleep(5)

    logger.debug('page loaded')
    return page


def get_next_page(page):
    """
    :type page: Tag
    :param page:
    :return:
    """
    next_page_tag = page.find(rel='next')
    if next_page_tag is None:
        return None
    else:
        return get_page('https://forum.ubuntu-fr.org/{0}'.format(next_page_tag.get('href')))


def is_noindex(post):
    """
    :type post: Tag
    :param post:
    :return: boolean
    """
    if re.search("%NOINDEX%", post.get_text()):
        return True
    else:
        return False


def get_photos_in_post(post):
    """
    :type post: Tag
    :param post:
    :return: list of photos or none
    """

    smileys = [
        'ubuntu-fr.org/img/smilies',
    ]
    message = post.find("div", class_="postmsg")

    photo_list = []
    has_photo = False
    logger.debug('Number of photos in this post: {0}'.format(len(message.select('span.postimg > img'))))
    for pict in message.select("span.postimg > img"):
        is_smiley = False
        is_in_quote = False
        photo_url = pict.get('src')
        for smiley in smileys:
            if smiley in photo_url:
                is_smiley = True
                break
        parent_tag = pict.parent
        while parent_tag:
            if parent_tag.has_attr('class'):
                if parent_tag['class'] == 'quotebox':
                    is_in_quote = True
                    break
                if parent_tag['class'] == 'postmsg':
                    break
            parent_tag = parent_tag.parent
        if not (is_smiley and is_in_quote):
            has_photo = True
            photo_url = photo_url
            photo_link_url = ''
            if pict.parent.parent.name == 'a':
                photo_link_url = pict.parent.parent.get('href')
            photo_list.append({
                'url': photo_url,
                'link_url': photo_link_url
            })

    if has_photo:
        user_name_tag = post.select('div.postleft > dl > dt > strong')
        logger.debug('User name: {0}'.format(type(user_name_tag[0])))
        user_name = user_name_tag[0].get_text()
        date_text = post.select("h2 > span > a")[0].get_text()
        logger.debug('Date: {0}'.format(date_text.encode('utf-8')))
        post_date_time = parse_topic_date(date_text)
        post_photos = {
            'id': int(post.get('id')[1:]),
            'user_name': user_name,
            'user_id': '',  # TO BE FILLED - NEED TO LOG ON...
            'date_time': post_date_time,
            'photos': photo_list,
        }
        return post_photos
    else:
        return None


def scrap_page(page, photo_collection):
    """

    :type page: BeautifulSoup
    :param page: list photos dictionary
    :type photo_collection: list
    :param photo_collection:
    :return:
    """
    logger.debug('Number of divs in this page: {0}'.format(len(page.findAll("div"))))
    logger.debug('Number of posts in this page: {0}'.format(len(page.findAll("div", class_="blockpost"))))
    for post in page.findAll("div", class_="blockpost"):
        photos = None
        if not is_noindex(post):
            photos = get_photos_in_post(post)
        if photos is not None:
            photo_collection.append(photos)

    return get_next_page(page), photo_collection


def scan_forum(url, is_new_topic):
    """
    :type url: str
    :param url:
    :type is_new_topic: bool
    :param is_new_topic: if false remove fist topic from the collection. Not implemented yet
    :return: list
    """
    logger.info('start...')
    try:
        page = get_page(url)
    except HTTPError as inst:
        if inst.code == 404:
            logger.warning('URL not valid, post potentially deleted.')
        raise
    except:
        logger.error('Impossible to process page')
        return None

    photo_collection = []
    while page:
        page, photo_collection = scrap_page(page, photo_collection)

    return photo_collection
