# Misc utilities #
import re
import logging
from datetime import date, timedelta


logger = logging.getLogger(__name__)


def assess_date_range(date_range):
    re_date = re.compile(r'^(?P<year>20\d{2})(/(?P<month>\d{1,2}))?(/('
                         r'?P<day>\d{1,2}))?$')
    match = re_date.match(date_range)
    if match:
        if match.group('day'):
            date_start = date(int(match.group('year')), int(match.group(
                'month')), int(match.group('day')))
            date_end = date_start + timedelta(days=1) - timedelta(
                microseconds=1)
        elif match.group('month'):
            date_start = date(int(match.group('year')),int(match.group(
                'month')), 1)
            date_end = date(int(match.group('year')), int(match.group(
                'month')) + 1, 1) - timedelta(microseconds=1)
        else:
            date_start = date(int(match.group('year')),1,1)
            date_end  = date(int(match.group('year')),12,31)
        return date_start, date_end
    else:
        raise ValueError('Value {} is not a valid date'.format(date_range))

