# -*- coding: utf-8 -*-

from django.test import TestCase
from datetime import datetime, timedelta
from utils.managephotos import parse_topic_date
# Create your tests here.
class ManagePhotosTests(TestCase):
    def test_parse_topic_date_today(self):
        str_date = u"Aujourd'hui à 11:36"
        test_date = datetime.now()
        test_date = test_date.replace(hour=11, minute=36, second=0, microsecond=0)
        self.assertEquals(parse_topic_date(str_date), test_date)

    def test_parse_topic_date_yesterday(self):
        str_date = u"Hier à 23:41"
        test_date = datetime.now() - timedelta(days=1)
        test_date = test_date.replace(hour=23, minute=41, second=0, microsecond=0)
        self.assertEquals(parse_topic_date(str_date), test_date)

    def test_parse_topic_date_older(self):
        str_date = u"Le 22/05/2016, à 21:47"
        test_date = datetime(year=2016, month=5, day=22, hour=21, minute=47, second=0, microsecond=0)
        self.assertEquals(parse_topic_date(str_date), test_date)


