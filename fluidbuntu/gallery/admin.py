from __future__ import absolute_import, unicode_literals

#from django import forms
from django.contrib import admin
#from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
#from django.contrib.auth.forms import UserChangeForm, UserCreationForm

# Register your models here.
from .models import User as FluidUser, Photo, Tag, Camera, Manufacturer, Topic

#class FluidUser(admin.ModelAdmin):
#    fieldsets = [name,]

admin.site.register(FluidUser)
admin.site.register(Photo)
admin.site.register(Tag)
admin.site.register(Camera)
admin.site.register(Manufacturer)
admin.site.register(Topic)
