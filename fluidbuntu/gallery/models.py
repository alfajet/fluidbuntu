from __future__ import unicode_literals
import logging
from unidecode import unidecode  # Used to slugify usernames

from django.db import models
from django.dispatch import receiver
from django.utils.text import slugify
from django.core.urlresolvers import reverse
# from django.db.models import

from .utils.scanforum import scan_forum, get_page
from .utils.managephotos import \
    get_photo_files, EXIF_TAG_NAMES, EXIF_TAGS, EXIF_DATA_TYPE
#    set_upload_path_lr
from datetime import datetime
from urllib2 import HTTPError

logger = logging.getLogger(__name__)
FORUM_BASE_URL = 'https://forum.ubuntu-fr.org/viewtopic.php?pid='

PHOTO_SIZES = {
    'LR': 640,  # Width or Height for the small image
    'HR': 2000,  # Max Width or Height for the large image
}


def set_photo_path(instance, filename):
    """
    :type instance: Photo
    :type filename: str
    :rtype: str
    """
    try:
        photo_date = instance.post_date
    except:
        photo_date = datetime.now()
    path = 'photo/{0}/{1:02d}/'.format(photo_date.year,
                                       photo_date.month) + filename
    return path


# Methods kept for backward compatibility
# To be removed after having squashed migrations
def set_upload_path_hr(instance, filename):
    return set_photo_path(instance, filename)


def set_upload_path_lr(instance, filename):
    return set_photo_path(instance, filename)


class User(models.Model):
    forum_id = models.IntegerField(null=True)  # forum user id
    name = models.CharField(max_length=255)

    def get_absolute_url(self):
        return reverse('gallery:author', kwargs={
            'slug': self.slug(),
            'id': str(self.id)})

    def slug(self):
        return slugify(unidecode(self.name))

    def _is_used(self):
        """
        Checks if user has related photos.
        :rtype: bool
        """
        return Photo.objects.filter(author=self).exists()

    is_used = property(_is_used)

    objects = models.Manager

    def __str__(self):
        return unicode(self.name).encode('utf-8')


class Unit(models.Model):
    name = models.CharField(max_length=63)
    symbol = models.CharField(max_length=20)


class Attribute(models.Model):
    DATA_TYPE_CHOICES = (
        (0, 'Short text (<255 characters)'),
        (1, 'Long text'),
        (2, 'Point (float) value'),
        (3, 'Date time'),
    )

    name = models.CharField(max_length=255)
    attribute_type = models.IntegerField(choices=DATA_TYPE_CHOICES)
    unit = models.ForeignKey(Unit, null=True)

    def __str__(self):
        return self.name


class Layout(models.Model):
    name = models.CharField(max_length=255)
    attributes = models.ManyToManyField(
        Attribute,
        through='LayoutItems',
        through_fields=('layout', 'attribute'),
    )


class LayoutItems(models.Model):
    attribute = models.ForeignKey(Attribute, on_delete=models.CASCADE)
    layout = models.ForeignKey(Layout, on_delete=models.CASCADE)

    order = models.IntegerField()


class Data(models.Model):
    attribute = models.ForeignKey(Attribute)
    # photo = models.ForeignKey('Photo')
    shorttxt = models.CharField(max_length=255)
    longtxt = models.TextField()
    point = models.FloatField()
    datetime = models.DateTimeField()


class Manufacturer(models.Model):
    name = models.CharField(max_length=255)

    @property
    def is_used(self):
        """
        Checks if user has related photos.
        :rtype: bool
        """
        return not Camera.objects.filter(manufacturer=self).exists()

    def __str__(self):
        return self.name


class Camera(models.Model):
    name = models.CharField(max_length=255)
    manufacturer = models.ForeignKey(Manufacturer, related_name='camera_set')

    @property
    def is_used(self):
        """
        Checks if user has related photos.
        :rtype: bool
        """
        return not Photo.objects.filter(camera=self).exists()

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(max_length=255)
    # How to enforce uniqueness within a branch?
    parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name


class Photo(models.Model):
    # SIZE_LARGE = 2000  # Max Width or Height for the large image
    # SIZE_SMALL = 640  # Width or Height for the small image

    id = models.CharField(max_length=255, primary_key=True)
    # ID generated at import
    post_id = models.IntegerField()
    author = models.ForeignKey(User, related_name='photo_set')
    camera = models.ForeignKey(Camera, null=True, blank=True,
                               related_name='photo_set')
    # Add links to TAGS - should be a many to many relation
    post_date = models.DateTimeField(null=True, blank=True)
    photo_date = models.DateTimeField(null=True, blank=True)
    added_date = models.DateTimeField(auto_now_add=True, blank=True)

    photo_low_res = models.ImageField(max_length=255, blank=True,
                                      upload_to=set_photo_path)
    photo_high_res = models.ImageField(max_length=255, blank=True,
                                       upload_to=set_photo_path)
    # post_id = models.IntegerField #Ubuntu ID - check if integer is OK

    tags = models.ManyToManyField(Tag, blank=True)
    technical_details = models.ManyToManyField(Data, blank=True)

    topic = models.ForeignKey('Topic', null=True)

    def get_absolute_url(self):
        return reverse('gallery:single_photo', kwargs={
            'uslug': self.author.slug(),
            'id':self.id,
        })

    # @property
    # def slug(self):
    #     return slugify(unidecode(self.author.name + "_" + self.id))
        # return self.author.slug() + unidecode("_" + id)

    def _get_post_url(self):
        return '{0}{1}#p{1}'.format(FORUM_BASE_URL, self.post_id)

    post_url = property(_get_post_url)

    # Check for different field type
    # Full URL or reconstruct from ID and user preference (u|ku|xu)

    # Technical attributes

    def add_author(self, username):
        user, created = User.objects.get_or_create(name=username)
        if created:
            user.save()
        self.author = user

    def __str__(self):
        return self.post_url

    def process_exif(self, exif):
        """
        Add exif to
        :param exif:
        :type exif: dict
        :return:
        """
        if exif[EXIF_TAG_NAMES.Make] and exif[EXIF_TAG_NAMES.Model]:
            make, created = Manufacturer.objects.get_or_create(
                name=exif[EXIF_TAG_NAMES.Make])
            if created:
                make.save()

            model = Camera.objects.get_or_create(name=exif[
                EXIF_TAG_NAMES.Model], manufacturer=make)
            if created:
                model.manufacturer = make
                model.save()

        for key in exif:
            if not (key == EXIF_TAG_NAMES.Make or key == EXIF_TAG_NAMES.Model):
                self.add_data(key, EXIF_TAGS[key]['type'], exif[key])

    def add_data(self, attribute_name, attribute_type, datum):
        attribute, created = Attribute.objects.get_or_create(
            name=attribute_name)
        if created:
            attribute.attribute_type = attribute_type
            attribute.save()
        d = Data()
        d.attribute = attribute
        if attribute_type == EXIF_DATA_TYPE.Short_text:
            d.shorttxt = str(datum)
        elif attribute_type == EXIF_DATA_TYPE.Long_text:
            d.longtxt = str(datum)
        elif attribute_type == EXIF_DATA_TYPE.Point:
            d.point = float(datum) #Todo: add error handling
        elif attribute_type == EXIF_DATA_TYPE.DateTime:
            d.datetime == datum #Todo: add date checker
        else:
            raise ValueError
        d.save()
        self.technical_details.add(d)



class Topic(models.Model):
    post_id = models.IntegerField()
    name = models.CharField(null=True, max_length=255)
    last_import_date = models.DateTimeField(null=True, auto_now=True)
    last_post_id_imported = models.IntegerField(null=True, blank=True)

    def _get_topic_url(self):
        return '%s%s' % (FORUM_BASE_URL, self.post_id)

    topic_url = property(_get_topic_url)

    def _get_last_imported_url(self):
        if self.last_post_id_imported:
            return '%s%s' % (FORUM_BASE_URL, self.last_post_id_imported)
        else:
            return None

    last_imported_url = property(_get_last_imported_url)

    def run_import(self):
        logger.info('Start import...')
        previous_import = self.last_imported_url
        if previous_import:
            last_post_url = previous_import
            is_new_topic = False
            logger.info('Updating existing topic')
        else:
            last_post_url = self.topic_url
            is_new_topic = True
            logger.info('New topic.')

        photo_collection = []
        try:
            photo_collection = scan_forum(last_post_url, is_new_topic)
        except HTTPError as inst:
            if inst.code == 404:
                self.clean_photos()
                logger.warning('Invalid photo removed. Scan aborted.')
                return None

        logger.info('{0} topics fetched.'.format(len(photo_collection)))
        self.import_photos(photo_collection)
        self.last_post_id_imported = photo_collection[-1]['id']
        self.save()

    def import_photos(self, photo_collection):
        for post in photo_collection:
            if not Photo.objects.filter(post_id=post['id']).exists():
                for index, photo in enumerate(post['photos']):
                    # sizes = [Photo.SIZE_SMALL, Photo.SIZE_LARGE]
                    photo_id = '{0}_{1}'.format(post['id'], str(index))
                    try:
                        photo_files, exif_data = \
                            get_photo_files(photo_id, photo['url'],
                                            photo['link_url'], PHOTO_SIZES)
                    except:
                        logger.warn('Impossible to load photo {}, it will be'
                                    'skipped'.format(photo['url']))
                    else:
                        p = Photo()
                        p.post_id = post['id']
                        p.id = photo_id
                        p.add_author(post['user_name'])
                        # logger.debug('Photo saved as: {0}.'.format(
                        #     p.photo_low_res.path))
                        p.post_date = post['date_time']
                        p.photo_low_res.save(photo_files['LR'][0],
                                             photo_files['LR'][1])
                        p.photo_high_res.save(photo_files['HR'][0],
                                              photo_files['HR'][1])
                        p.topic = self
                        p.save()
                        # ensure to save the post id each time a photo is added
                        self.last_post_id_imported = post['id']
                        self.save()

    def clean_photos(self):
        """
        Clear latest photos having an invalid post_id until a photo with a
        valid post_id is found.

        This feature is required to avoid the scan to crash in the event when
        the post of the latest scanned photo has been deleted from the thread.
        """
        logger.info('Cleaning photo thread...')
        topic_photos = Photo.objects.filter(topic=self).order_by('-post_id')
        photos_to_delete = []
        for p in topic_photos:
            url = '%s%s' % (FORUM_BASE_URL, p.post_id)
            try:
                get_page(url)
            except HTTPError as inst:
                if inst.code == 404:
                    photos_to_delete.append(p)
                    logger.debug('Photo {} to be deleted'.find(p.id))
                pass
            else:
                if len(photos_to_delete) > 0:
                    for ptd in photos_to_delete:
                        ptd.delete()
                    self.last_post_id_imported = p.post_id
                    logger.debug('New last post id: {}'.format(p.post_id))
                    self.save()
                break

    def purge(self):
        """Clear all pictures related to this topic and related objects"""
        photos_to_delete = Photo.objects.filter(topic=self)
        logger.info('Found {0} photos to be deleted'.format(len(
            photos_to_delete)))
        for p in photos_to_delete:
            photos_to_delete.delete()
        self.last_post_id_imported = None
        self.last_import_date = None

    def __str__(self):
        # return unicode(selast_post_id_importedlf.name).encode('utf-8')
        return self.name


@receiver(models.signals.pre_delete, sender=Photo,
          dispatch_uid='photo_predelete_signal')
def remove_photo_files(sender, instance, **kwargs):
    # deleted_photo = kwargs.get('instance')
    logger.info('Deleting photo file: {0}.'.format(
        instance.photo_low_res))
    instance.photo_low_res.delete(save=False)
    logger.info('Deleting photo file: {0}.'.format(
        instance.photo_high_res))
    instance.photo_high_res.delete(save=False)


@receiver(models.signals.post_delete, sender=Photo,
          dispatch_uid='photo_postdelete_signal')
def clear_empty_objects(sender, instance, **kwargs):
    """Removing any left-over objects: User, Camera"""
    #    User.objects.filter(photo_set=None).delete()
    Camera.objects.filter(photo_set=None).delete()
    Manufacturer.objects.filter(camera_set=None).delete()
