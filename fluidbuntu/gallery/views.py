from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, Http404
from django.template import loader
# from django.core.paginator import Paginator, InvalidPage
from django.core import serializers
from el_pagination.decorators import page_template
from utils.misc import assess_date_range

from .models import Photo, User
# , User, Unit, Attribute, Layout, LayoutItems, Data, Manufacturer, Camera, Tag


# Create your views here.
@page_template('gallery/entry_list_page.html')  # just add this decorator
def index(request, template='gallery/index.html', extra_context=None):
    context = {
        'photo_list': Photo.objects.all().order_by('-added_date').select_related('author', 'camera__manufacturer'),
        'user_list': User.objects.all().order_by('name'),
        # 'page': page_photo,
    }

    if extra_context is not None:
        context.update(extra_context)
    return render(request, template, context)


@page_template('gallery/entry_list_page.html')  # just add this decorator
def author(request, slug, id, template='gallery/index.html', \
                                     extra_context=None):
    author = get_object_or_404(User, pk=id)

    context = {
        'photo_list': Photo.objects.all().filter(author_id=id).order_by(
            '-added_date').select_related('author', 'camera__manufacturer'),
        'user_list': User.objects.all(),
        'author': author,
    }
    if extra_context is not None:
        context.update(extra_context)
    return render(request, template, context)


def author_list(request, template='gallery/authors.html', extra_context=None):
    context = {
        'author_list': User.objects.all().order_by('name'),
    }

    if extra_context is not None:
        context.update(extra_context)
    return render(request, template, context)


def date(request, template='gallery/date.html', extra_context=None):
    context = {
        'photo_list': Photo.objects.all().order_by('post_date'),
    }

    if extra_context is not None:
        context.update(extra_context)
    return render(request, template, context)

@page_template('gallery/entry_list_page.html')
def date_range(request, date_range, template='gallery/index.html', \
               extra_context=None):
    try:
        start_date, end_date = assess_date_range(date_range)
    except ValueError:
        raise Http404('Date range not valid.')

    context = {
        'photo_list': Photo.objects.all().filter(
            post_date__range=[start_date, end_date])
            .order_by('post_date').select_related('author',
                                                  'camera__manufacturer'),
        'user_list': User.objects.all().order_by('name'),
        }

    if extra_context is not None:
        context.update(extra_context)
    return render(request, template, context)


@page_template('gallery/entry_list_page.html')  # just add this decorator
def single_photo(request, uslug, id, template='gallery/index.html', \
                                      extra_context=None):
    photo = get_object_or_404(
        Photo.objects.select_related('author', 'camera__manufacturer'), id=id)
    context = {
        'photo_list': [photo,],
        'user_list': User.objects.all(),
    }

    if extra_context is not None:
        context.update(extra_context)
    return render(request, template, context)
